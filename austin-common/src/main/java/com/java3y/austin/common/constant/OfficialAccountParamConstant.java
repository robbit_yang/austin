package com.java3y.austin.common.constant;


/**
 * @author 3y
 * 微信服务号的参数常量
 */
public class OfficialAccountParamConstant {
    public static final String SIGNATURE = "signature";
    public static final String ECHO_STR = "echostr";
    public static final String NONCE = "nonce";
    public static final String TIMESTAMP = "timestamp";
    public static final String ENCRYPT_TYPE = "encrypt_type";
    public static final String RAW = "raw";
    public static final String AES = "aes";
    public static final String MSG_SIGNATURE = "msg_signature";
}
